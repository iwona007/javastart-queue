package Iwona.pl;


import Iwona.pl.exception.NoSuchOptionException;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Scanner;

public class TaskManager {

    private Queue<Task> listToDo = new PriorityQueue<>();
    private Scanner sc = new Scanner(System.in);

   public void optionLoop() {
        Option option;
        do {
            printOption();
            System.out.println("Wybierz ocje");
            option = Option.createFromInt(sc.nextInt());
            sc.nextLine();
            switch (option) {
                case ADD:
                    listToDo.offer(readAndCreateTask());
                    System.out.println("Zadanie dodano do kolejki");
                    break;
                case TAKE:
                    takeTask();
                    break;
                case EXIT:
                    System.out.println("Wyjście z programu: Papa");
                    break;
                default:
                    System.out.println("Nie rozpoznana opcja");
            }
        } while (option != Option.EXIT);
    }

    private Task readAndCreateTask() {
        System.out.println("Podaj nazwę zadania:");
        String name = sc.nextLine();
        System.out.println("Podaj opis zadania:");
        String description = sc.nextLine();
        System.out.print("Podaj priorytet, ");
        for (Task.Priority priority : Task.Priority.values()) {
            System.out.println(priority + ", ");
        }
        System.out.println();
        Task.Priority priority = Task.Priority.valueOf(sc.nextLine());
        return new Task(name, description, priority);
    }

   private void takeTask() {
        if (listToDo.isEmpty()) {
            System.out.println("Nie ma zadań");
        } else {
            System.out.println("Zadanie do wykonania:");
            Task taskToDo = listToDo.poll();
            System.out.println(taskToDo);
        }
    }

    private void printOption() {
        for (Option option : Option.values()) {
            System.out.println(option.toString());
        }
    }

    private enum Option {
        ADD(0, " - dodaj zadanie"),
        TAKE(1, " - pobierz zadanie"),
        EXIT(2, " - zakończ program");

        private int number;
        private String description;

        Option(int number, String description) {
            this.number = number;
            this.description = description;
        }

        static Option createFromInt(int option) {
            try {
                return Option.values()[option];
            } catch (ArrayIndexOutOfBoundsException ex) {
                throw new NoSuchOptionException("Brak opcji o id " + option);
            }
        }

        @Override
        public String toString() {
            return number + " " + description;
        }
    }
}
